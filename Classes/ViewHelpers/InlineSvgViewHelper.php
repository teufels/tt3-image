<?php
namespace Teufels\Tt3Image\ViewHelpers;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use InvalidArgumentException;

/**
 * Class InlineSvgViewHelper.
 **/
class InlineSvgViewHelper extends AbstractViewHelper
{
    const SELECT_STYLE_ATTRIBUTE_REGEX = ['/(style=\".*?\")/'];

    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('filePath', 'string', 'Absolute path to file', true);
        $this->registerArgument('removeStyleAttributes', 'bool', 'Remove Style Attributes', false, false);
        $this->registerArgument('additionalRemoveTagRegex', 'array', 'Additional Remove Tag Regex', false, []);
        $this->registerArgument('alt', 'string', 'Alternative text', false, '');
        $this->registerArgument('role', 'string', 'Role attribute for SVG', false, 'img');
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $filePath = $this->arguments['filePath'];
        $removeStyleAttributes = $this->arguments['removeStyleAttributes'];
        $additionalRemoveTagRegex = $this->arguments['additionalRemoveTagRegex'];
        $altText = $this->arguments['alt'] ?? '';
        $role = $this->arguments['role'] ?? 'img';

        $absoluteFilePath = Environment::getPublicPath() . $filePath;

        if (!file_exists($absoluteFilePath)) {
            throw new InvalidArgumentException("File *$absoluteFilePath* does not exist on the server.");
        }

        $content = file_get_contents($absoluteFilePath);

        if ($removeStyleAttributes) {
            $regex = array_merge(self::SELECT_STYLE_ATTRIBUTE_REGEX, $additionalRemoveTagRegex);
            $content = preg_replace($regex, '', $content);
        }

        // Inject attributes into the <svg> tag & return render string
        return $this->addAttributesToSvg($content, $altText, $role);
    }

    /**
     * Adds attributes to an inline SVG content
     *
     * @param string $content SVG content as a string.
     * @param string $altText Alternative text
     * @param string $role Role attribute
     * @return string Updated SVG content with attributes.
     */
    protected function addAttributesToSvg(string $content, string $altText, string $role): string
    {
        // Remove existing XML declaration if present
        $svgContent = preg_replace('/<\?xml[^>]+\?>/i', '', $content);

        // Add XML declaration for DOMDocument processing
        $svgContent = '<?xml version="1.0" encoding="utf-8"?>' . $svgContent;

        $dom = new \DOMDocument();
        $dom->loadXML($svgContent);

        // Get the SVG element
        $svgElement = $dom->getElementsByTagName('svg')->item(0);

        // Check if SVG element exists
        if ($svgElement) {
            if ($role === 'presentation') {
                // For decorative SVGs, add role="presentation" and aria-hidden="true"
                $svgElement->setAttribute('role', 'presentation');
                $svgElement->setAttribute('aria-hidden', 'true');
            } else {
                // For meaningful SVGs, add role="img" and aria-label with alt text
                $svgElement->setAttribute('role', 'img');
                $svgElement->setAttribute('aria-label', $altText);
            }

            // Return updated SVG content without additional XML declaration
            return $dom->saveXML($dom->documentElement);
        } else {
            throw new InvalidArgumentException("SVG tag not found in the content.");
        }
    }
}
