<?php
$customColumns = [
    'rendering' => [
        'displayCond' => 'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isSVG',
        'exclude' => true,
        'label' => 'Rendering',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Default', 0, ''],
                ['Inline', 1, ''],
                ['File', 2, ''],
            ],
            'default' => 0,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ]
        ]
    ],
    'processing' => [
        'displayCond' => 'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isImage',
        'exclude' => true,
        'label' => 'Processing',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Default', 0, ''],
                ['no processing', 1, ''],
            ],
            'default' => 0,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ]
        ]
    ],
    'imagelazyload' => [
        'displayCond' => [
            'OR' => [
                'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isImage',
                'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isSVG',
            ],
        ],
        'exclude' => true,
        'label' => 'Lazy load images',
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'items' => [
                [
                    0 => '',
                    1 => ''
                ]
            ],
            'default' => 1,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ]
        ]
    ],
    'decorative' => [
        'displayCond' => [
            'OR' => [
                'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isImage',
                'USER:Teufels\\Tt3Image\\UserFunc\\CheckFile->isSVG',
            ],
        ],
        'exclude' => true,
        'label' => 'decorative',
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'items' => [
                [
                    0 => '',
                    1 => ''
                ]
            ],
            'default' => 0,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ]
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'sys_file_reference',
    $customColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_file_reference',
    'imageoverlayPalette',
    'rendering, processing, imagelazyload, decorative'
);
