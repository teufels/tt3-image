<?php
defined('TYPO3') or die('Access denied.');

$extensionKey = 'tt3_image';
$extensionTitle = '[teufels] Image';

// If automatically include of TypoScript is disabled, then you can include it in the (BE) static-template select-box
if (isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'])
    && $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'] === '0') {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);
}