[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--image-orange.svg)](https://bitbucket.org/teufels/tt3-image/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__image-red.svg)](https://bitbucket.org/teufels/tt3-image/src/main/)
![version](https://img.shields.io/badge/version-1.5.*-yellow.svg?style=flat-square)

[ ṯeufels ] Image
==========
Provide Viewhelpers, Crop Variants and SVG (inline/file) Support

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-image`

***

### Requirements
- none

***

### How to use
- Install with composer
- (Optional) Import Static Template (before sitepackage)
    - will be automatically imported - on problems or need of changed order include manual

***

### How to use (ViewHelper)
```
{namespace tt3Image=Teufels\Tt3Image\ViewHelpers}
<f:format.raw>
    <tt3Image:inlineSvg filePath="/absolute/path/to/some_file.svg" removeStyleAttributes="1" />
</f:format.raw>
```

***

### Migration from hive_image
- migrate from old ViewHelper `hiveImage` to `tt3Image`, see above how to use ViewHelper

***

### Changelog
#### 1.5.x
- 1.5.2 fix wrong role='img' inside img-tag (file) on fileSVG
- 1.5.1 Migrate configuration path from plugin.tx_tt3_image to plugin.tx_tt3image while maintaining backwards compatibility
- 1.5.0 add own ViewHelper for JSON Decode to remove the addiction to `fluidtypo3/vhs`
#### 1.4.x
- 1.4.0 TYPO3 v13 support
#### 1.3.x
- 1.3.0 add necessary accessibility attributes. Implement toggle option for 'decorative' images.
#### 1.2.x
- 1.2.0 add toggle for possibility to disable lazy loading
#### 1.1.x
- 1.1.0 put maxWidth image size in Constants to change on purpose
- 1.1.0 change breakpoint for largest image (1920) from 1280 to 1200
#### 1.0.x
- 1.0.2 stucture code
- 1.0.1 cleanup TypoScript / compress Code
- 1.0.0 intial from [hive_image](https://bitbucket.org/teufels/hive_image/src/main/)