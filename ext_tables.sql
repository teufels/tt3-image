CREATE TABLE sys_file_reference (
    rendering tinyint(4) DEFAULT '0' NOT NULL,
    processing tinyint(4) DEFAULT '0' NOT NULL,
    imagelazyload int(1) DEFAULT '1' NOT NULL,
    decorative int(1) DEFAULT '0' NOT NULL
);